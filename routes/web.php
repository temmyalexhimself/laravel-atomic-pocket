<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){
    Route::get('wallets/show/all', 'WalletController@all_wallet')->name('wallets.all');
    Route::get('wallets/show/active', 'WalletController@wallet_active')->name('wallets.active');
    Route::get('wallets/show/not-active', 'WalletController@wallet_not_active')->name('wallets.not-active');
    Route::patch('wallets/update/status/{id}/{status}', 'WalletController@update_status')->name('wallets.update.status');
    Route::resource('wallets', 'WalletController');

    Route::get('categories/show/all', 'CategoryController@all_category')->name('categories.all');
    Route::get('categories/show/active', 'CategoryController@category_active')->name('categories.active');
    Route::get('categories/show/not-active', 'CategoryController@category_not_active')->name('categories.not-active');
    Route::patch('categories/update/status/{id}/{status}', 'CategoryController@update_status')->name('categories.update.status');
    Route::resource('categories', 'CategoryController');

    Route::get('transactions/create/wallet-in', 'TransactionController@create_wallet_in')->name('transactions.create.wallet-in');
    Route::get('transactions/show/wallet-in', 'TransactionController@wallet_in')->name('transactions.wallet-in');
    Route::post('transactions/process/wallet-in', 'TransactionController@process_wallet_in')->name('transactions.process.wallet-in');

    Route::get('transactions/create/wallet-out', 'TransactionController@create_wallet_out')->name('transactions.create.wallet-out');
    Route::get('transactions/show/wallet-out', 'TransactionController@wallet_out')->name('transactions.wallet-out');
    Route::post('transactions/process/wallet-out', 'TransactionController@process_wallet_out')->name('transactions.process.wallet-out');

    Route::get('report/transactions/view', 'ReportController@view_report')->name('reports.view');
    Route::get('report/transactions/show', 'ReportController@show_report')->name('reports.show');
});
