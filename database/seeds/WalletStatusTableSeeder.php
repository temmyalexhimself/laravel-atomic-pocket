<?php

use App\WalletStatus;
use Illuminate\Database\Seeder;

class WalletStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WalletStatus::create([
            'name' => 'Dompet Utama'
        ]);

        WalletStatus::create([
            'name' => 'Dompet Tagihan'
        ]);

        WalletStatus::create([
            'name' => 'Dompet Cadangan'
        ]);
    }
}
