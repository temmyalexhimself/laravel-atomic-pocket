<?php

use App\CategoryStatus;
use Illuminate\Database\Seeder;

class CategoryStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryStatus::create([
            'name' => 'Pengeluaran'
        ]);

        CategoryStatus::create([
            'name' => 'Pemasukan'
        ]);

        CategoryStatus::create([
            'name' => 'Lain-lain'
        ]);
    }
}
