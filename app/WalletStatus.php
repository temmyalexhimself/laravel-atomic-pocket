<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletStatus extends Model
{
    protected $guarded = ['id'];
}
