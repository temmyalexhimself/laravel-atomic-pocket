<?php

namespace App\Exports;

use App\Transaction;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransactionExport implements FromView, ShouldAutoSize
{
    public $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function view(): View
    {
        $params = $this->params;

        $start_date = $params['start_date'];
        $end_date = $params['end_date'];
        $wallet_id = $params['wallet_id'];
        $category_id = $params['category_id'];

        if ($category_id == 'all_category' && $wallet_id == 'all_wallet') {
            $transactions = Transaction::orderBy('created_at', 'desc')
                ->filterDate($start_date, $end_date)
                ->get();

        } else if ($category_id == 'all_category') {
            $transactions = Transaction::filterWallet($params)
                ->filterDate($start_date, $end_date)
                ->get();
        } else if ($wallet_id == 'all_wallet') {
            $transactions = Transaction::filterCategory($params)
                ->filterDate($start_date, $end_date)
                ->get();
        } else {
            $transactions = Transaction::filterCategory($params)
                ->filterWallet($params)
                ->filterDate($start_date, $end_date)
                ->get();
        }

        return view('report.excel', compact('transactions'));
    }
}
