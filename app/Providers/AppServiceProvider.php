<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        $wallet_in = DB::table('transactions')
                ->select(DB::raw('MAX(id) as wallet_in'))
                ->where('category_id', 2)
                ->get();

        $wallet_out = DB::table('transactions')
                ->select(DB::raw('MAX(id) as wallet_out'))
                ->where('category_id', 1)
                ->get();

        $code_in = $wallet_in[0]->wallet_in == NULL ? (int) 1 : $wallet_in[0]->wallet_in += 1;
        $lpad_code_in = str_pad($code_in, 5,'0',STR_PAD_LEFT);
        $wallet_in_code = 'WIN'.$lpad_code_in.'1';

        $code_out = $wallet_out[0]->wallet_out == NULL ? (int) 1 : $wallet_out[0]->wallet_out += 1;
        $lpad_code_out = str_pad($code_out, 5,'0',STR_PAD_LEFT);
        $wallet_out_code = 'WIN'.$lpad_code_out.'2';

        View::share([
            'wallet_in_code' => $wallet_in_code,
            'wallet_out_code' => $wallet_out_code
        ]);
    }
}
