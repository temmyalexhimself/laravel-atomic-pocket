<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exports\TransactionExport;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\CountValidator\Exact;

class ReportController extends Controller
{
    public function view_report()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        $wallets = Wallet::orderBy('name', 'asc')->get();

        return view('report.view_report', [
            'categories' => $categories,
            'wallets' => $wallets,
        ]);
    }

    public function show_report(Request $request)
    {
        $params = $request->except('_token');
        $start_date = $params['start_date'];
        $end_date = $params['end_date'];
        $wallet_id = $params['wallet_id'];
        $category_id = $params['category_id'];

        if ($request->submit_button == 'show_report') {
            if ($category_id == 'all_category' && $wallet_id == 'all_wallet') {
                $transactions = Transaction::orderBy('created_at', 'desc')
                    ->filterDate($start_date, $end_date)
                    ->get();

            } else if ($category_id == 'all_category') {
                $transactions = Transaction::filterWallet($params)
                    ->filterDate($start_date, $end_date)
                    ->get();
            } else if ($wallet_id == 'all_wallet') {
                $transactions = Transaction::filterCategory($params)
                    ->filterDate($start_date, $end_date)
                    ->get();
            } else {
                $transactions = Transaction::filterCategory($params)
                    ->filterWallet($params)
                    ->filterDate($start_date, $end_date)
                    ->get();
            }

            return view('report.show_report', [
                'transactions' => $transactions,
                'start_date' => $start_date,
                'end_date' => $end_date,
            ]);

        } else if ($request->submit_button == 'export_excel') {
            $now = date('Y-m-d');
            return Excel::download(new TransactionExport($params), 'report-transaction'.$now.'.xlsx');
        }
    }
}
