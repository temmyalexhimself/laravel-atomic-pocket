<?php

namespace App\Http\Controllers;

use App\Category;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function wallet_in()
    {
        $transactions = Transaction::where('category_id', 2)->get();

        return view('transactions.wallet_in', [
            'transactions' => $transactions
        ]);
    }

    public function create_wallet_in()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        $wallets = Wallet::orderBy('name', 'asc')->get();

        return view('transactions.create_wallet_in', [
            'categories' => $categories,
            'wallets' => $wallets
        ]);
    }

    public function process_wallet_in(Request $request)
    {
        $request->validate([
            'value' => 'required|min:1',
            'wallet_id' => 'required',
            'category_id' => 'required'
        ]);

        Transaction::create([
            'code' => $request->code,
            'date' => $request->date,
            'category_id' => $request->category_id,
            'wallet_id' => $request->wallet_id,
            'value' => $request->value,
            'description' => $request->description
        ]);

        return redirect()->route('transactions.wallet-in')->with('success', 'Transaksi dompet masuk berhasil disimpan!');
    }

    public function wallet_out()
    {
        $transactions = Transaction::where('category_id', 1)->get();

        return view('transactions.wallet_out', [
            'transactions' => $transactions
        ]);
    }

    public function create_wallet_out()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        $wallets = Wallet::orderBy('name', 'asc')->get();

        return view('transactions.create_wallet_out', [
            'categories' => $categories,
            'wallets' => $wallets
        ]);
    }

    public function process_wallet_out(Request $request)
    {
        $request->validate([
            'value' => 'required|min:1',
            'wallet_id' => 'required',
            'category_id' => 'required'
        ]);

        Transaction::create([
            'code' => $request->code,
            'date' => $request->date,
            'category_id' => $request->category_id,
            'wallet_id' => $request->wallet_id,
            'value' => $request->value,
            'description' => $request->description
        ]);

        return redirect()->route('transactions.wallet-out')->with('success', 'Transaksi dompet keluar berhasil disimpan!');
    }
}
