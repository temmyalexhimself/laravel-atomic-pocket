<?php

namespace App\Http\Controllers;

use App\Wallet;
use App\WalletStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_all = DB::table('wallets')->count();
        $count_active = DB::table('wallets')->where('status', 1)->count();
        $count_not_active = DB::table('wallets')->where('status', 2)->count();

        $wallets = Wallet::all();

        return view('wallets.index', [
            'wallets' => $wallets,
            'count_not_active' => $count_not_active,
            'count_active' => $count_active,
            'count_all' => $count_all
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wallet_statuses = WalletStatus::orderBy('name', 'asc')
                            ->get();

        return view('wallets.create', [
            'wallet_statuses' => $wallet_statuses
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'max:100',
            'wallet_status_id' => 'required',
            'status' => 'required'
         ]);

         Wallet::create([
            'name' => $request->name,
            'reference' => $request->reference,
            'description' => $request->description,
            'wallet_status_id' => $request->wallet_status_id,
            'status' => $request->status
         ]);

         return redirect()->route('wallets.index')->with('success', 'Data dompet berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wallet = Wallet::find($id);
        return view('wallets.show', ['wallet' => $wallet]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wallet = Wallet::find($id);
        $wallet_statuses = WalletStatus::orderBy('name', 'asc')
                            ->get();

        return view('wallets.edit', [
            'wallet' => $wallet,
            'wallet_statuses' => $wallet_statuses
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $wallet = Wallet::find($id);
        $wallet->update([
            'name' => $request->name,
            'reference' => $request->reference,
            'description' => $request->description,
            'wallet_status_id' => $request->wallet_status_id
        ]);

        return redirect()->route('wallets.index')->with('success', 'Data dompet berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_status(Request $request, $id, $status)
    {
        $wallet_status = Wallet::find($id);
        $wallet_status->update([
            'status' => $status
        ]);

        return redirect()->route('wallets.index')->with('success', 'Status dompet berhasil diubah!');
    }

    public function wallet_active()
    {
        $wallets = DB::table('wallets')
            ->where('status', 1)
            ->get();

        $count_all = DB::table('wallets')
                        ->count();

        $count_active = DB::table('wallets')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('wallets')
            ->where('status', 2)
            ->count();

        return view('wallets.active', [
            'wallets' => $wallets,
            'count_not_active' => $count_not_active,
            'count_active' => $count_active,
            'count_all' => $count_all
        ]);
    }

    public function wallet_not_active()
    {
        $wallets = DB::table('wallets')
            ->where('status', 2)
            ->get();

        $count_all = DB::table('wallets')
                        ->count();

        $count_active = DB::table('wallets')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('wallets')
            ->where('status', 2)
            ->count();

        return view('wallets.active', [
            'wallets' => $wallets,
            'count_active' => $count_active,
            'count_not_active' => $count_not_active,
            'count_all' => $count_all
        ]);
    }

    public function all_wallet()
    {
        $wallets = DB::table('wallets')
                ->get();

        $count_all = DB::table('wallets')
                    ->count();

        $count_active = DB::table('wallets')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('wallets')
            ->where('status', 2)
            ->count();

        return view('wallets.all_wallet', [
            'wallets' => $wallets,
            'count_active' => $count_active,
            'count_not_active' => $count_not_active,
            'count_all' => $count_all
        ]);
    }
}
