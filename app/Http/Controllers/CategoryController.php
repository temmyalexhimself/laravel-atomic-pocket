<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryStatus;
use App\WalletStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_all = DB::table('categories')->count();

        $count_active = DB::table('categories')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('categories')
            ->where('status', 2)
            ->count();

        $categories = Category::all();

        return view('categories.index', [
            'categories' => $categories,
            'count_not_active' => $count_not_active,
            'count_active' => $count_active,
            'count_all' => $count_all
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_statuses = CategoryStatus::orderBy('name', 'asc')->get();
        return view('categories.create', ['category_statuses' => $category_statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5',
            'description' => 'max:100',
            'category_status_id' => 'required',
            'status' => 'required'
        ]);

        Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'category_status_id' => $request->category_status_id,
            'status' => $request->status
        ]);

        return redirect()->route('categories.index')->with('success', 'Data kategori berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        return view('categories.show', [
            'category' => $category
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $category_statuses = CategoryStatus::orderBy('name', 'asc')->get();

        return view('categories.edit', [
            'category' => $category,
            'category_statuses' => $category_statuses
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $request->validate([
            'name' => 'required|min:5',
            'description' => 'max:100',
            'category_status_id' => 'required',
            'status' => 'required'
        ]);

        $category->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_status_id' => $request->category_status_id
        ]);

        return redirect()->route('categories.index')->with('success', 'Data kategori berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function update_status(Request $request, $id, $status)
    {
        $category_status = Category::find($id);
        $category_status->update([
            'status' => $status
        ]);

        return redirect()->route('categories.index')->with('success', 'Status dompet berhasil diubah!');
    }

    public function category_active()
    {
        $categories = DB::table('categories')
            ->where('status', 1)
            ->get();

        $count_all = DB::table('categories')->count();

        $count_active = DB::table('categories')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('categories')
            ->where('status', 2)
            ->count();

        return view('categories.active', [
            'categories' => $categories,
            'count_active' => $count_active,
            'count_not_active' => $count_not_active,
            'count_all' => $count_all
        ]);
    }

    public function category_not_active()
    {
        $categories = DB::table('categories')
            ->where('status', 2)
            ->get();

        $count_all = DB::table('categories')->count();

        $count_active = DB::table('categories')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('categories')
            ->where('status', 2)
            ->count();

        return view('categories.not_active', [
            'categories' => $categories,
            'count_active' => $count_active,
            'count_not_active' => $count_not_active,
            'count_all' => $count_all
        ]);
    }

    public function all_category()
    {
        $categories = DB::table('categories')
                ->get();

        $count_all = DB::table('categories')->count();

        $count_active = DB::table('categories')
            ->where('status', 1)
            ->count();

        $count_not_active = DB::table('categories')
            ->where('status', 2)
            ->count();

        return view('categories.not_active', [
            'categories' => $categories,
            'count_active' => $count_active,
            'count_not_active' => $count_not_active,
            'count_all' => $count_all
        ]);
    }
}
