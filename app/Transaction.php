<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    protected $guarded = ['id'];

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeFilterCategory($query, $params)
    {
        if(isset($params['category_id']) && trim($params['category_id'] !== ''))
        {
            $query->where('category_id', trim($params['category_id']));
        }

        return $query;
    }

    public function scopeFilterWallet($query, $params)
    {
        if(isset($params['wallet_id']) && trim($params['wallet_id']) !== '')
        {
            $query->where('wallet_id', trim($params['wallet_id']));
        }

        return $query;
    }

    public function scopeFilterDate($query, $start_date, $end_date)
    {
        if(isset($start_date) && isset($end_date))
        {
            $query->whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date));
        }

        return $query;
    }
}
