@extends('layouts.master')

@section('title', 'Tambah Dompet')

@section('content')
<div class="container">
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET - Buat Baru</h1>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('wallets.index') }}" class="btn btn-primary">Kelola Dompet</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('wallets.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Nama <span>*</span></label>
                            <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                                value="{{ old('name') }}">

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="reference">Referensi</label>
                                <input id="reference" class="form-control" type="number"
                                    name="reference" value="{{ old('reference') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea id="description" class="form-control"
                        name="description" maxlength="100">{{ old('description') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="wallet_status_id">Category Status ID</label>
                    <select id="wallet_status_id" class="form-control col-md-6 @error('wallet_status_id') is-invalid @enderror" name="wallet_status_id">
                        <option value="">-- Pilih Status --</option>
                        @foreach ($wallet_statuses as $wallet_status)
                            <option value="{{ $wallet_status->id }}"
                                {{ old('wallet_status_id') == $wallet_status->id ? 'selected' : '' }}>
                                {{ $wallet_status->name }}
                            </option>
                        @endforeach
                    </select>

                    @error('wallet_status_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select id="status" class="form-control col-md-6" name="status">
                        <option value="">-- Pilih Status --</option>
                        <option value="1" selected>Aktif</option>
                        <option value="2">Tidak Aktif</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-user">
                    Submit
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
