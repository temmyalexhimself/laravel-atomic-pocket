@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-wallet tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'Tambah Dompet')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-7">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET - Aktif</h1>
                </div>
                <div class="col-md-5">
                    <a href="{{ route('wallets.create') }}" class="btn btn-primary">Buat Baru</a>
                    <a href="{{ route('wallets.all') }}" class="btn btn-secondary">Semua ({{ $count_all }})</a>
                    <a href="{{ route('wallets.active') }}" class="btn btn-success">Aktif ({{ $count_active }})</a>
                    <a href="{{ route('wallets.not-active') }}" class="btn btn-danger">Tidak Aktif ({{ $count_not_active }})</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-wallet">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NAMA</th>
                        <th>REFERENSI</th>
                        <th>DESKRIPSI</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($wallets as $wallet)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $wallet->name }}</td>
                            <td>{{ $wallet->reference }}</td>
                            <td>{{ $wallet->description }}</td>
                            <td>{{ $wallet->status == 1 ? 'Aktif' : 'Tidak Aktif' }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li class="mb-2">{{ $wallet->name }}</li>
                                      <li><a href="{{ route('wallets.show', $wallet->id) }}"><i class="fas fa-search" style="color: #3056c8;"></i> Detail</a></li>
                                      <li><a href="{{ route('wallets.edit', $wallet->id) }}"><i class="fas fa-edit" style="color: #3056c8;"></i> Ubah</a></li>
                                      <li>
                                          @php
                                              $status = $wallet->status == 1 ? 2 : 1;
                                          @endphp
                                          <form action="{{ route('wallets.update.status', [$wallet->id, $status]) }}" method="POST">
                                            @csrf
                                            @method('PATCH')

                                            @if ($wallet->status == 1)
                                                <button type="submit" class="btn btn-primary">Tidak Aktif</button>
                                            @elseif($wallet->status == 2)
                                                <button type="submit" class="btn btn-primary">Aktif</button>
                                            @endif
                                          </form>
                                      </li>
                                    </ul>
                                  </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js"></script> --}}
    <script>
        $(document).ready( function () {
            $('#table-wallet').DataTable();
        });
    </script>
@endpush
