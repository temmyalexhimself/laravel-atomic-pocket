@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-wallet tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'Tambah Dompet')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET - Aktif</h1>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('wallets.index') }}" class="btn btn-primary">Kelola Dompet</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-wallet">
                <thead>
                    <tr>
                        <th>NAMA</th>
                        <th>REFERENSI</th>
                        <th>DESKRIPSI</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $wallet->name }}</td>
                        <td>{{ $wallet->reference }}</td>
                        <td>{{ $wallet->description }}</td>
                        <td>{{ $wallet->status == 1 ? 'Aktif' : 'Tidak Aktif' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js"></script> --}}
    <script>
        $(document).ready( function () {
            $('#table-wallet').DataTable();
        });
    </script>
@endpush
