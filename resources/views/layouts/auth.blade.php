@include('layouts.include.login.head')

<body class="bg-gradient-primary">

  <div class="container">

    @yield('content')
  </div>

@include('layouts.include.login.script')
