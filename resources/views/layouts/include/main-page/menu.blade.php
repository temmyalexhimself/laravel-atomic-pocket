 <!-- Heading -->
 <div class="sidebar-heading">
     Menu
 </div>

 <!-- Nav Item - Pages Collapse Menu -->
 <li class="nav-item">
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
         aria-controls="collapseTwo">
         <i class="fas fa-fw fa-cog"></i>
         <span>Master</span>
     </a>
     <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
         <div class="bg-white py-2 collapse-inner rounded">
             <a class="collapse-item" href="{{ route('wallets.index') }}">Dompet</a>
             <a class="collapse-item" href="{{ route('categories.index') }}">Kategori</a>
         </div>
     </div>
 </li>

 <!-- Nav Item - Utilities Collapse Menu -->
 <li class="nav-item">
     <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true"
         aria-controls="collapseUtilities">
         <i class="fas fa-fw fa-wrench"></i>
         <span>Transaksi</span>
     </a>
     <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
         <div class="bg-white py-2 collapse-inner rounded">
             <a class="collapse-item" href="{{ route('transactions.wallet-in') }}">Dompet Masuk</a>
             <a class="collapse-item" href="{{ route('transactions.wallet-out') }}">Dompet Keluar</a>
         </div>
     </div>
 </li>

  <!-- Nav Item - Utilities Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReport" aria-expanded="true"
        aria-controls="collapseReport">
        <i class="fas fa-download"></i>
        <span>Laporan</span>
    </a>
    <div id="collapseReport" class="collapse" aria-labelledby="headingReport" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('reports.view') }}">Transaksi</a>
        </div>
    </div>
</li>
