<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Maju Bersama Atomic Indonesia &copy; 2020</span>
        </div>
    </div>
</footer>
