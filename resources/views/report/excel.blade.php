<table>
    <thead>
        <tr>
            <th>#</th>
            <th>TANGGAL</th>
            <th>KODE</th>
            <th>DESKRIPSI</th>
            <th>DOMPET</th>
            <th>KATEGORI</th>
            <th>NILAI</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($transactions as $transaction)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $transaction->date }}</td>
                <td>{{ $transaction->code }}</td>
                <td>{{ $transaction->description }}</td>
                <td>{{ $transaction->wallet->name }}</td>
                <td>{{ $transaction->category->name }}</td>
                <td>{{  $transaction->category_id == 1 ? '(-)' : '(+)' }} {{ number_format($transaction->value) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
