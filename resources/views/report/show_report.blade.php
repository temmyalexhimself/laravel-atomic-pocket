@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-transaction tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'List Dompet Masuk')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h1 class="h3 mb-2 text-gray-800">RIWAYAT TRANSAKSI</h1>
                    <h5>{{ $start_date }} s/d {{ $end_date }}</h5>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('reports.view') }}" class="btn btn-primary">Kembali</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-transaction">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TANGGAL</th>
                        <th>KODE</th>
                        <th>DESKRIPSI</th>
                        <th>DOMPET</th>
                        <th>KATEGORI</th>
                        <th>NILAI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $transaction->date }}</td>
                            <td>{{ $transaction->code }}</td>
                            <td>{{ $transaction->description }}</td>
                            <td>{{ $transaction->wallet->name }}</td>
                            <td>{{ $transaction->category->name }}</td>
                            <td>{{  $transaction->category_id == 1 ? '(-)' : '(+)' }} {{ number_format($transaction->value) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#table-transaction').DataTable();
        });
    </script>
@endpush
