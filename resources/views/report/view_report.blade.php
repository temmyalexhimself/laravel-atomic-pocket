@extends('layouts.master')

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
@endpush

@section('title', 'Cetak Laporan')

@section('content')
<div class="container">
    <div class="card mb-4">
        <div class="card-header">
            <h1 class="h3 mb-2 text-gray-800">LAPORAN Transaksi</h1>
        </div>
        <div class="card-body">
            <h3 class="mb-4">TRANSAKSI</h3>
            <form action="{{ route('reports.show') }}" method="GET">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="code">Tanggal Awal</label>
                            <input type="text" name="start_date" class="form-control start_date @error('start_date') has-error @enderror"
                                placeholder="YYYY-MM-DD" value="{{ old('start_date', date('Y-m-d')) }}">

                            @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="code">Tanggal Akhir</label>
                            <input type="text" name="end_date" class="form-control end_date @error('end_date') has-error @enderror"
                                placeholder="YYYY-MM-DD" value="{{ old('end_date', date('Y-m-d')) }}">

                            @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="category_id">Kategori</label>
                            <select id="category_id" class="form-control" name="category_id">
                                <option value="">-- Pilih Kategori --</option>
                                <option value="all_category">Semua</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="wallet_id">Dompet</label>
                            <select id="wallet_id" class="form-control" name="wallet_id">
                                <option value="">-- Pilih Dompet --</option>
                                <option value="all_wallet">Semua</option>
                                @foreach ($wallets as $wallet)
                                    <option value="{{ $wallet->id }}">{{ $wallet->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary btn-user" name="submit_button" value="show_report">
                    Buat Laporan
                </button>
                <button type="submit" class="btn btn-primary btn-user" name="submit_button" value="export_excel">
                    Buat Excel
                </button>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

<script>
    // $(function(){
	// 	$("#datepicker").datepicker({
	// 		todayHighlight: true,
	// 		format: 'd-M-yyyy'
	// 	});
	// });

    $(function () {
        $('.start_date').datepicker({
            format: 'yyyy-mm-dd',
            orientation: 'top auto'
        });

        $('.end_date').datepicker({
            format: 'yyyy-mm-dd',
            orientation: 'top auto'
        });
    });
</script>
@endpush
