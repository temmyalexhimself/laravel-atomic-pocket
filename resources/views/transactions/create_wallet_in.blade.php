@extends('layouts.master')

@section('title', 'Tambah Kategori')

@section('content')
<div class="container">
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET MASUK - Buat Baru</h1>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('transactions.wallet-in') }}" class="btn btn-primary">Kelola Dompek Masuk</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('transactions.process.wallet-in') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="code">Kode</label>
                            <input id="code" class="form-control @error('code') is-invalid @enderror" type="text"
                                name="code" value="{{ old('code', $wallet_in_code) }}" readonly>

                            @error('code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="date">Tanggal</label>
                            <input id="date" class="form-control @error('date') is-invalid @enderror" type="text"
                                name="date" value="{{ old('date', date('Y-m-d')) }}" readonly>

                            @error('date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="category_id">Kategori:</label>
                            <select id="category_id" class="form-control @error('category_id') is-invalid @enderror" name="category_id">
                                <option value="">-- Pilih Kategori</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->name == 'Pemasukan' ? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>

                            @error('category_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="wallet_id">Dompet<span>*</span></label>
                            <select id="wallet_id" class="form-control @error('wallet_id') is-invalid @enderror" name="wallet_id">
                                <option value="">-- Pilih Dompet --</option>
                                @foreach ($wallets as $wallet)
                                    <option value="{{ $wallet->id }}" {{ $wallet->name == 'Dompet Utama' ? 'selected' : '' }}>{{ $wallet->name }}</option>
                                @endforeach
                            </select>

                            @error('wallet_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="value">Nilai</label>
                    <input id="value" class="form-control col-md-6 @error('value') is-invalid @enderror" type="number" name="value" min="0" value="{{ old('value') }}"
                        onkeyup="if(this.value < 0) this.value=1"
                        onblur="if(this.value<  0) this.value=1">

                        @error('value')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                </div>

                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea id="description" class="form-control" name="description"
                        maxlength="100">{{ old('description') }}</textarea>
                </div>

                <button type="submit" class="btn btn-primary btn-user">
                    Submit
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
