@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-transaction tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'List Dompet Masuk')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET MASUK</h1>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('transactions.create.wallet-in') }}" class="btn btn-primary">Buat Baru</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-transaction">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>TANGGAL</th>
                        <th>KODE</th>
                        <th>DESKRIPSI</th>
                        <th>NILAI</th>
                        <th>DOMPET</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $transaction->date }}</td>
                            <td>{{ $transaction->code }}</td>
                            <td>{{ $transaction->description }}</td>
                            <td>{{ number_format($transaction->value) }}</td>
                            <td>{{ $transaction->wallet->name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js"></script> --}}
    <script>
        $(document).ready( function () {
            $('#table-transaction').DataTable();
        });
    </script>
@endpush
