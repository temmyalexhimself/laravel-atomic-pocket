@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-wallet tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'List Kategori')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-7">
                    <h1 class="h3 mb-2 text-gray-800">DOMPET - Aktif</h1>
                </div>
                <div class="col-md-5">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary">Buat Baru</a>
                    <a href="{{ route('categories.active') }}" class="btn btn-success">Aktif ({{ $count_active }})</a>
                    <a href="{{ route('categories.not-active') }}" class="btn btn-danger">Tidak Aktif ({{ $count_not_active }})</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-categories">
                <thead>
                    <tr>
                        <th>NAMA</th>
                        <th>DESKRIPSI</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $category[0]->name }}</td>
                        <td>{{ $category[0]->description }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js"></script> --}}
    <script>
        $(document).ready( function () {
            $('#table-categories').DataTable();
        });
    </script>
@endpush
