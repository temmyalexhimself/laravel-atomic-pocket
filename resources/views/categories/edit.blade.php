@extends('layouts.master')

@section('title', 'Ubah Kategori')

@section('content')
<div class="container">
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="h3 mb-2 text-gray-800">KATEGORI - Ubah</h1>
                </div>
                <div class="col-md-3">
                    <a href="{{ route('categories.index') }}" class="btn btn-primary">Kelola Kategori</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('categories.update', $category->id) }}" method="POST">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label for="name">Nama <span>*</span></label>
                    <input id="name" class="form-control col-md-6 @error('name') is-invalid @enderror" type="text" name="name"
                        value="{{ old('name', $category->name) }}">

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea id="description" class="form-control"
                        name="description" maxlength="100">{{ old('description', $category->description) }}</textarea>
                </div>

                <div class="form-group">
                    <label for="category_status_id">Category Status ID</label>
                    <select id="category_status_id" class="form-control col-md-6 @error('category_status_id') is-invalid @enderror" name="category_status_id">
                        <option value="">-- Pilih Status --</option>
                        @foreach ($category_statuses as $category_status)
                            <option value="{{ $category_status->id }}"
                                {{ old('category_status_id') == $category_status->id ? 'selected' : '' }}
                                {{ $category->category_status_id == $category_status->id ? 'selected' : '' }}>
                                {{ $category_status->name }}
                            </option>
                        @endforeach
                    </select>

                    @error('category_status_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="status">Status</label>
                    <select id="status" class="form-control col-md-6" name="status">
                        <option value="">-- Pilih Status --</option>
                        <option value="1" {{ $category->status == 1 ? 'selected' : '' }}>Aktif</option>
                        <option value="2" {{ $category->status == 2 ? 'selectd' : '' }}>Tidak Aktif</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-user">
                    Submit
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
