@extends('layouts.master')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">
    <style>
        #table-wallet tr th {
            background-color: #3056c8;
            color: #fff !important;
        }

        .dropdown-menu {
            padding: 1em;
        }
    </style>
@endpush

@section('title', 'List Kategori')

@section('content')
<div class="container">
    @include('layouts.include.alert')
    <div class="card mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-7">
                    <h1 class="h3 mb-2 text-gray-800">KATEGORI - Aktif</h1>
                </div>
                <div class="col-md-5">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary">Buat Baru</a>
                    <a href="{{ route('categories.all') }}" class="btn btn-secondary">Semua ({{ $count_all }})</a>
                    <a href="{{ route('categories.active') }}" class="btn btn-success">Aktif ({{ $count_active }})</a>
                    <a href="{{ route('categories.not-active') }}" class="btn btn-danger">Tidak Aktif ({{ $count_not_active }})</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="table-categories">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NAMA</th>
                        <th>DESKRIPSI</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->description }}</td>
                            <td>{{ $category->status == 1 ? 'Aktif' : 'Tidak Aktif' }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                      <li class="mb-2">{{ $category->name }}</li>
                                      <li><a href="{{ route('categories.show', $category->id) }}"><i class="fas fa-search" style="color: #3056c8;"></i> Detail</a></li>
                                      <li><a href="{{ route('categories.edit', $category->id) }}"><i class="fas fa-edit" style="color: #3056c8;"></i> Ubah</a></li>
                                      <li>
                                          @php
                                              $status = $category->status == 1 ? 2 : 1;
                                          @endphp
                                          <form action="{{ route('categories.update.status', [$category->category_status_id, $status]) }}" method="POST">
                                            @csrf
                                            @method('PATCH')

                                            @if ($category->status == 1)
                                                <button type="submit" class="btn btn-primary">Tidak Aktif</button>
                                            @elseif($category->status == 2)
                                                <button type="submit" class="btn btn-primary">Aktif</button>
                                            @endif
                                          </form>
                                      </li>
                                    </ul>
                                  </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap.min.js"></script> --}}
    <script>
        $(document).ready( function () {
            $('#table-categories').DataTable();
        });
    </script>
@endpush
